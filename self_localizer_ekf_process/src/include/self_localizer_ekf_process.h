#include <iostream>
#include <string>
#include <math.h>
#include <mutex>
#include <binders.h>
#include <thread>

//ROS
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PointStamped.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "std_msgs/Float32.h"

//robot process
#include "robot_process.h"

//Eigen
#include "eigen3/Eigen/Eigen"
#include "eigen3/Eigen/Eigen"

//tf
#include "tf_conversions/tf_eigen.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

//ros time synchronizer
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//dynamic reconfigure
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>

class self_localizer_ekf_process : public RobotProcess
{

public:
    self_localizer_ekf_process();
    ~self_localizer_ekf_process();

public:
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();

private:
    ros::NodeHandle n;

    //Subscibers
    ros::Subscriber dji_velocity_sub_;
    ros::Subscriber dji_imu_sub_;
    ros::Subscriber dji_height_sub_;
    ros::Subscriber msf_odom_sub_;
    ros::Subscriber optitrack_odom_sub_;

    void msfOdomCallback(const nav_msgs::Odometry& msg);
    void optitrackOdomCallback(const nav_msgs::OdometryConstPtr& msg);

    tf::TransformListener listener_;

    //message synchronizers for gps and imu
    message_filters::Subscriber<sensor_msgs::Imu> dji_imu_snyn_sub_;
    message_filters::Subscriber<nav_msgs::Odometry> gps_enu_sub_;

    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Imu,
    nav_msgs::Odometry> SyncPolicy;
    message_filters::Synchronizer<SyncPolicy> sync;

    void imuGPSSyncCallback(const sensor_msgs::Imu &imu_msg,
                            const nav_msgs::Odometry &gps_msg);


    //message synchronizers for vo and imu
    //message_filters::Subscriber<sensor_msgs::Imu> dji_vo_imu_sync_sub_;
    //message_filters::Subscriber<self_localizer_ekf_process::VOPosition> vo_pose_sub_;

    //Publishers
    ros::Subscriber speed_sub;
    void speedCallback(const geometry_msgs::TwistStamped& msg);
    ros::Subscriber imu_sub;
    void imuCallback(const sensor_msgs::Imu& msg);
    ros::Subscriber altitude_sub;
    void altitudeCallback(const geometry_msgs::PointStamped& msg);

    ros::Publisher velocity_pub_;
    ros::Publisher pose_pub_;
    ros::Publisher altitude_pub_;

    ros::Publisher msf_odom_pub_;
    inline void publishMSFOdom();


    nav_msgs::Odometry msf_odom_;
    inline void setMSFOdom(nav_msgs::Odometry odom);
    inline void getMSFOdom(nav_msgs::Odometry& odom);

    ros::Publisher aerostack_pose_pub_;
    ros::Publisher aerostack_speed_pub_;

    inline void setIMU(sensor_msgs::Imu imu);
    inline sensor_msgs::Imu getIMU();
    sensor_msgs::Imu imu_;

    double vo_position_x_, vo_position_y_, vo_position_z_, altitude_;


    //publisher thread
private:
    std::thread *msf_odom_pub_thread;
    std::mutex msf_odom_lock_;


private:
    bool tello_first_yaw_measurement_;
    double tello_first_roll_, tello_first_pitch_, tello_first_yaw_;

    bool dji_first_yaw_measurement_;
    double dji_first_roll_, dji_first_pitch_, dji_first_yaw_;
    double prev_time_; double current_time_;
    bool start_calcualating_dt_;

private:

    bool use_vo_data_, use_global_positioning_data_;
    bool use_rtk_position_, use_rtk_pose_, use_gps_pose_;
    bool use_optitrack_data_;
    bool is_first_vo_z_msg_;
    float first_vo_z_msg_;

    tf::Quaternion getCorrectYaw(sensor_msgs::Imu imu_msg);
    Eigen::Vector3f twistToENU(geometry_msgs::TwistStamped msg);
    Eigen::Vector3f ConvertToENU(geometry_msgs::Vector3Stamped msg);
};





