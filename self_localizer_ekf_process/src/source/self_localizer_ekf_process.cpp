#include "self_localizer_ekf_process.h"

self_localizer_ekf_process::self_localizer_ekf_process():
    RobotProcess(), sync (SyncPolicy(10))
{
    std::cout << "self_localizer_ekf_process constructor " << std::endl;
}

self_localizer_ekf_process::~self_localizer_ekf_process()
{
    std::cout << "self_localizer_ekf_process destructor " << std::endl;
}


void self_localizer_ekf_process::ownSetUp()
{
    //assign all the variables here

    //initializing the filter
    this->init();
}

void self_localizer_ekf_process::ownStop()
{
    //assign all the variables to default here


}

void self_localizer_ekf_process::ownRun()
{
    //run the algorithm over here

}

void self_localizer_ekf_process::init()
{

    dji_first_yaw_measurement_ = false;
    tello_first_yaw_measurement_ = true;
    is_first_vo_z_msg_         = false;
    start_calcualating_dt_     = false;
    dji_first_roll_ =0; dji_first_pitch_ =0; dji_first_yaw_ =0;
    prev_time_ = 0; current_time_ = 0;
    vo_position_x_ = 0; vo_position_y_ = 0; vo_position_z_=0; altitude_=0;

    msf_odom_.pose.pose.position.x = 0; msf_odom_.pose.pose.position.y = 0; msf_odom_.pose.pose.position.z = 0;
    msf_odom_.pose.pose.orientation.x = 0; msf_odom_.pose.pose.orientation.y = 0; msf_odom_.pose.pose.orientation.z = 0; msf_odom_.pose.pose.orientation.w = 1;
    msf_odom_.twist.twist.linear.x = 0; msf_odom_.twist.twist.linear.y = 0; msf_odom_.twist.twist.linear.z = 0;
    msf_odom_.twist.twist.angular.x = 0; msf_odom_.twist.twist.angular.y = 0; msf_odom_.twist.twist.angular.z = 0;

    ros::param::param<bool>("~use_global_positioning_data", use_global_positioning_data_, false);
    ros::param::param<bool>("~use_vo_data", use_vo_data_, false);
    ros::param::param<bool>("~use_rtk_position", use_rtk_position_, false);
    ros::param::param<bool>("~use_rtk_pose", use_rtk_pose_, false);
    ros::param::param<bool>("~use_gps_pose", use_gps_pose_, false);
    ros::param::param<bool>("~use_optitrack_data", use_optitrack_data_, false);

    std::cout << "using global positioning data: " << use_global_positioning_data_ << std::endl;
    std::cout << "using VO data: " << use_vo_data_ << std::endl;
    std::cout << "using rtk position: " << use_rtk_position_ << std::endl;
    std::cout << "using rtk pose: " << use_rtk_pose_ << std::endl;
    std::cout << "using gps pose: " << use_gps_pose_ << std::endl;
    std::cout << "using optitrack data: " << use_optitrack_data_ << std::endl;


}

void self_localizer_ekf_process::ownStart()
{
    //start all the subscribers and publishers over here
    speed_sub = n.subscribe("sensor_measurement/speed",1, &self_localizer_ekf_process::speedCallback, this);
    imu_sub = n.subscribe("sensor_measurement/imu",1,&self_localizer_ekf_process::imuCallback, this);
    altitude_sub = n.subscribe("sensor_measurement/altitude",1,&self_localizer_ekf_process::altitudeCallback, this);

    if(use_global_positioning_data_)
    {
        if(use_vo_data_)
            ROS_WARN("Tried to activate gps pose data and VO pose data which is prohibited, not publishing gps data");
        else
        {
            //time synchronization of GPS and imu
            dji_imu_snyn_sub_.subscribe(n, "dji_sdk/imu",1);
            if(use_rtk_pose_)
            {
                gps_enu_sub_.subscribe(n, "rtk_position_enu",1);
                ROS_INFO("Subscribing to RTK Pose data");
            }
            else if (use_gps_pose_)
            {
                gps_enu_sub_.subscribe(n, "gps_position_enu",1);
                ROS_INFO("Subscribing to GPS Pose data");

            }
            if(use_rtk_pose_ && use_gps_pose_)
            {
                ROS_WARN("Tried to activate both gps pose data and rtk pose data which is prohibited, publishing only gps data");
                gps_enu_sub_.subscribe(n, "gps_position_enu",1);
            }
        }
    }
    else if(use_optitrack_data_)
    {
        if(use_vo_data_)
            ROS_WARN("Tried to activate optitrack pose data and VO pose data which is prohibited, not publishing optitrack data");
        else if(use_rtk_pose_ || use_gps_pose_)
            ROS_WARN("Tried to activate optitrack pose data and gps/rtk pose data which is prohibited, not publishing optitrack data");
        else
            optitrack_odom_sub_  = n.subscribe("/vrpn_client_node/m210/pose",1, &self_localizer_ekf_process::optitrackOdomCallback, this);
    }

    pose_pub_      = n.advertise<geometry_msgs::PoseStamped>("pose",1);
    velocity_pub_  = n.advertise<geometry_msgs::PointStamped>("velocity", 1);
    altitude_pub_  = n.advertise<geometry_msgs::PointStamped>("altitude",1);

    msf_odom_sub_  = n.subscribe("/msf_core/odometry", 1, &self_localizer_ekf_process::msfOdomCallback, this);
    msf_odom_pub_  = n.advertise<nav_msgs::Odometry>("odometry",1);
    
    aerostack_pose_pub_  = n.advertise<geometry_msgs::PoseStamped>("self_localization/pose",1);
    aerostack_speed_pub_ = n.advertise<geometry_msgs::TwistStamped>("self_localization/speed",1);

    //thread for publishing msf at given hz
    msf_odom_pub_thread = new std::thread(&self_localizer_ekf_process::publishMSFOdom, this);

    //waiting for the msf to be launched first
    usleep(5000000);
    //starting the ethz msf
    dynamic_reconfigure::ReconfigureRequest srv_req;
    dynamic_reconfigure::ReconfigureResponse srv_resp;
    dynamic_reconfigure::BoolParameter bool_param;
    dynamic_reconfigure::Config conf;

    bool_param.name = "core_init_filter";
    bool_param.value = true;
    conf.bools.push_back(bool_param);

    srv_req.config = conf;
    ros::service::call("/msf_pose_position_velocity_altitude_sensor/pose_velocity_altitude_sensor/set_parameters", srv_req, srv_resp);
}

void self_localizer_ekf_process::speedCallback(const geometry_msgs::TwistStamped &msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] speedCallback" << std::endl;

    std::cout << "SPEED PUBLISHED: " << msg.twist.linear.x << ", " << msg.twist.linear.y << ", " << msg.twist.linear.z << std::endl;

    if(tello_first_yaw_measurement_)
        return;

    current_time_ = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

    geometry_msgs::PointStamped velocity_world;
    velocity_world.header    = msg.header;
    velocity_world.point.x   = msg.twist.linear.x;
    velocity_world.point.y   = msg.twist.linear.y;
    velocity_world.point.z   = msg.twist.linear.z;

    velocity_pub_.publish(velocity_world);

    if(!start_calcualating_dt_)
    {
        start_calcualating_dt_ = true;
        prev_time_ = current_time_ ;
        return;
    }

    double time_diff = current_time_ - prev_time_;

    sensor_msgs::Imu curr_imu_data;
    curr_imu_data = this->getIMU();
    tf::Quaternion quaternion = this->getCorrectYaw(curr_imu_data);

    vo_position_x_ = vo_position_x_ + msg.twist.linear.x*time_diff;
    vo_position_y_ = vo_position_y_ + msg.twist.linear.y*time_diff;
    //vo_position_z_ = vo_position_z_ + msg.twist.linear.z*time_diff;
    vo_position_z_ = altitude_;

    geometry_msgs::PoseStamped vo_pose;
    vo_pose.header.stamp    = msg.header.stamp;
    vo_pose.header.frame_id = msg.header.frame_id;
    vo_pose.pose.position.x = vo_position_x_;
    vo_pose.pose.position.y = vo_position_y_;
    vo_pose.pose.position.z = vo_position_z_;
    vo_pose.pose.orientation.x = quaternion.getX();
    vo_pose.pose.orientation.y = quaternion.getY();
    vo_pose.pose.orientation.z = quaternion.getZ();
    vo_pose.pose.orientation.w = quaternion.getW();

    pose_pub_.publish(vo_pose);
    prev_time_ = current_time_;
}


void self_localizer_ekf_process::imuGPSSyncCallback(const sensor_msgs::Imu &imu_msg,
                                              const nav_msgs::Odometry &gps_msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] imuGPSSyncCallback" << std::endl;
    if(!dji_first_yaw_measurement_)
        return;

    geometry_msgs::PoseStamped gps_imu_pose;
    gps_imu_pose.header.stamp = imu_msg.header.stamp;
    gps_imu_pose.header.frame_id = gps_msg.header.frame_id;

    //adding position data
    gps_imu_pose.pose.position.x = gps_msg.pose.pose.position.x;
    gps_imu_pose.pose.position.y = gps_msg.pose.pose.position.y;
    gps_imu_pose.pose.position.z = gps_msg.pose.pose.position.z;

    tf::Quaternion quaternion = this->getCorrectYaw(imu_msg);

    //adding orientation data
    gps_imu_pose.pose.orientation.x = quaternion.getX();
    gps_imu_pose.pose.orientation.y = quaternion.getY();
    gps_imu_pose.pose.orientation.z = quaternion.getZ();
    gps_imu_pose.pose.orientation.w = quaternion.getW();

    pose_pub_.publish(gps_imu_pose);
}

void self_localizer_ekf_process::optitrackOdomCallback(const nav_msgs::OdometryConstPtr &msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] optitrackOdomCallback" << std::endl;
    geometry_msgs::PoseStamped opti_pose;
    opti_pose.header.stamp = msg->header.stamp;
    opti_pose.pose.position.x = msg->pose.pose.position.x;
    opti_pose.pose.position.y = msg->pose.pose.position.y;
    opti_pose.pose.position.z = msg->pose.pose.position.z;

    opti_pose.pose.orientation.x = msg->pose.pose.orientation.x;
    opti_pose.pose.orientation.y = msg->pose.pose.orientation.y;
    opti_pose.pose.orientation.z = msg->pose.pose.orientation.z;
    opti_pose.pose.orientation.w = msg->pose.pose.orientation.w;

    pose_pub_.publish(opti_pose);
}

void self_localizer_ekf_process::imuCallback(const sensor_msgs::Imu &msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] imuCallback" << std::endl;

    std::cout << "ORIENTATION PUBLISHED: " << msg.orientation.x << ", " << msg.orientation.y << ", " << msg.orientation.z << std::endl;
    std::cout << "ANG SPEED PUBLISHED: " << msg.angular_velocity.x << ", " << msg.angular_velocity.y << ", " << msg.angular_velocity.z << std::endl;
    std::cout << "ACCELERATION PUBLISHED: " << msg.linear_acceleration.x << ", " << msg.linear_acceleration.y << ", " << msg.linear_acceleration.z << std::endl;

    if(!tello_first_yaw_measurement_)
    {
        tf::Quaternion q(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w);
        tf::Matrix3x3 m(q);

        //convert quaternion to euler angels
        m.getRPY(dji_first_roll_, dji_first_pitch_, dji_first_yaw_);

        if(std::isnan(dji_first_yaw_))
            return;

        tello_first_yaw_measurement_ = true;
    }

    this->setIMU(msg);

    return;
}


void self_localizer_ekf_process::setIMU(sensor_msgs::Imu imu)
{
    tello_first_yaw_measurement_ = false;
    imu_ = imu;
}

sensor_msgs::Imu self_localizer_ekf_process::getIMU()
{
    return imu_;
}

void self_localizer_ekf_process::altitudeCallback(const geometry_msgs::PointStamped &msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] altitudeCallback" << std::endl;
    std::cout << "ALTITUDE PUBLISHED: " << msg.point.x << ", " << msg.point.y << ", " << msg.point.z << std::endl;

    geometry_msgs::PointStamped altitude;
    altitude.header.stamp = ros::Time::now();
    altitude.point.x      = 0;
    altitude.point.y      = 0;
    altitude.point.z      = msg.point.z;

    altitude_ = altitude.point.z;

    altitude_pub_.publish(altitude);
}

void self_localizer_ekf_process::msfOdomCallback(const nav_msgs::Odometry &msg)
{
    std::cout << "[ETHZ_MSG_PROCESS] msfOdomCallback" << std::endl;
    std::cout << "POSE PUBLISHED: " << msg.pose.pose.position.x << ", " << msg.pose.pose.position.y << ", " << msg.pose.pose.position.z << std::endl;
    this->setMSFOdom(msg);
    
    geometry_msgs::PoseStamped estimated_pose;
    estimated_pose.header.stamp = msg.header.stamp;
    estimated_pose.pose.position.x = msg.pose.pose.position.x;
    estimated_pose.pose.position.y = msg.pose.pose.position.y;
    estimated_pose.pose.position.z = msg.pose.pose.position.z;
    estimated_pose.pose.orientation.x = msg.pose.pose.orientation.x;
    estimated_pose.pose.orientation.y = msg.pose.pose.orientation.y;
    estimated_pose.pose.orientation.z = msg.pose.pose.orientation.z;
    estimated_pose.pose.orientation.w = msg.pose.pose.orientation.w;
    aerostack_pose_pub_.publish(estimated_pose);

    geometry_msgs::TwistStamped estimated_speed;
    estimated_speed.header.stamp = msg.header.stamp;
    estimated_speed.twist.linear.x = msg.twist.twist.linear.x;
    estimated_speed.twist.linear.y = msg.twist.twist.linear.y;
    estimated_speed.twist.linear.z = msg.twist.twist.linear.z;
    estimated_speed.twist.angular.x = msg.twist.twist.angular.x;
    estimated_speed.twist.angular.y = msg.twist.twist.angular.y;
    estimated_speed.twist.angular.z = msg.twist.twist.angular.z;
    aerostack_speed_pub_.publish(estimated_speed);

}

void self_localizer_ekf_process::setMSFOdom(nav_msgs::Odometry odom)
{
    msf_odom_lock_.lock();
    msf_odom_ = odom;
    msf_odom_lock_.unlock();
}

void self_localizer_ekf_process::getMSFOdom(nav_msgs::Odometry &odom)
{
    msf_odom_lock_.lock();
    odom = msf_odom_;
    msf_odom_lock_.unlock();
}


void self_localizer_ekf_process::publishMSFOdom()
{
    ros::Rate loop_rate(200);
    while(ros::ok())
    {
        nav_msgs::Odometry odom;
        this->getMSFOdom(odom);
        msf_odom_pub_.publish(odom);
        loop_rate.sleep();
    }
}


Eigen::Vector3f self_localizer_ekf_process::twistToENU(geometry_msgs::TwistStamped msg)
{
    //converting the proper ENU velocity frame to the aerostack world frame based on the first yaw measurement
    Eigen::Vector3f dji_proper_ENU_frame;
    Eigen::Vector3f local_world_frame;
    Eigen::Matrix3f local_world_rotation_mat;

    dji_proper_ENU_frame(0) = msg.twist.linear.x;
    dji_proper_ENU_frame(1) = msg.twist.linear.y;
    dji_proper_ENU_frame(2) = 0;

    local_world_rotation_mat(0,0) = cos(tello_first_yaw_);
    local_world_rotation_mat(1,0) = -sin(tello_first_yaw_);
    local_world_rotation_mat(2,0) = 0;

    local_world_rotation_mat(0,1) = sin(tello_first_yaw_);
    local_world_rotation_mat(1,1) = cos(tello_first_yaw_);
    local_world_rotation_mat(2,1) = 0;

    local_world_rotation_mat(0,2) = 0;
    local_world_rotation_mat(1,2) = 0;
    local_world_rotation_mat(2,2) = 1;

    local_world_frame = local_world_rotation_mat * dji_proper_ENU_frame;

    return local_world_frame;
}

Eigen::Vector3f self_localizer_ekf_process::ConvertToENU(geometry_msgs::Vector3Stamped msg)
{
    //converting the proper ENU velocity frame to the aerostack world frame based on the first yaw measurement
    Eigen::Vector3f dji_proper_ENU_frame;
    Eigen::Vector3f local_world_frame;
    Eigen::Matrix3f local_world_rotation_mat;

    dji_proper_ENU_frame(0) = msg.vector.x;
    dji_proper_ENU_frame(1) = msg.vector.y;
    dji_proper_ENU_frame(2) = 0;

    local_world_rotation_mat(0,0) = cos(dji_first_yaw_);
    local_world_rotation_mat(1,0) = -sin(dji_first_yaw_);
    local_world_rotation_mat(2,0) = 0;

    local_world_rotation_mat(0,1) = sin(dji_first_yaw_);
    local_world_rotation_mat(1,1) = cos(dji_first_yaw_);
    local_world_rotation_mat(2,1) = 0;

    local_world_rotation_mat(0,2) = 0;
    local_world_rotation_mat(1,2) = 0;
    local_world_rotation_mat(2,2) = 1;

    local_world_frame = local_world_rotation_mat * dji_proper_ENU_frame;

    return local_world_frame;
}

tf::Quaternion self_localizer_ekf_process::getCorrectYaw(sensor_msgs::Imu imu_msg)
{

    tf::Quaternion q(imu_msg.orientation.x, imu_msg.orientation.y, imu_msg.orientation.z, imu_msg.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    //convert quaternion to euler angels
    m.getRPY(roll, pitch, yaw);

    if(yaw - dji_first_yaw_ < -180.0)
        yaw = (yaw - dji_first_yaw_) + 360;
    else if(yaw - dji_first_yaw_ > 180)
        yaw = (yaw - dji_first_yaw_) - 360;
    else
        yaw = yaw - dji_first_yaw_;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll,pitch,yaw);

    return quaternion;

}
